import React, { Component } from 'react';
import Header from './Components/Header';
import TaskList from './Components/TaskList';
import {SortableContainer, arrayMove} from 'react-sortable-hoc';

import './App.css';
import './styles/commons.css';
import './styles/reset.css';

const SortableList = SortableContainer(TaskList)

class App extends Component {

    state = {
      taskList:[
        {id:Math.random(), text:'Go to fitness club', done:false},
        {id:Math.random(), text:'Go to Supermarket', done:false},
        {id:Math.random(), text:'Go to University', done:false},
        {id:Math.random(), text:'Go to Learn React!', done:false}
      ]
    };

  onSortEnd = ({oldIndex, newIndex}) => {
    this.setState({
      taskList: arrayMove(this.state.taskList, oldIndex, newIndex),
    });
  };

  addNewTask = (text) =>{
    const newTask = {
      id:Math.random(),
      text:text,
      done:false
    }
    this.setState({
      ...this.state,
      taskList: [...this.state.taskList, newTask]
    })
  }

  removeTask = (id) => {
    const newTaskList = [...this.state.taskList].filter(v => v.id !== id);

    this.setState({
      ...this.state,
      taskList: [...newTaskList]
    })
  }

  editTask = (id, value) => {
    const elementIndex = [...this.state.taskList].findIndex(v => v.id === id);

    const newTaskList = [...this.state.taskList];
    newTaskList[elementIndex].text = value;

    this.setState({
      ...this.state,
      taskList: [...newTaskList]
    })
  }

  switchStatus = (id) => {
    const elementIndex = [...this.state.taskList].findIndex(v => v.id === id);

    const newTaskList = [...this.state.taskList];
    newTaskList[elementIndex].done = !newTaskList[elementIndex].done;

    this.setState({
      ...this.state,
      taskList: [...newTaskList]
    })
  }
  render(){
    return(
        <div className="app-container">
          <div className="app">
            <Header addTask={this.addNewTask}/>
            <SortableList
                {...this.state}
                removeTask={this.removeTask}
                switchStatus = {this.switchStatus}
                editTask = {this.editTask}
                items={this.state.taskList} onSortEnd={this.onSortEnd}
            />
          </div>
        </div>
    )
  }

}





export default App;